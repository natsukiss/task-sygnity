function Avatar() {
    this.initAvatarGenerator = function () {
        this.assignOldImageValue();
        this.initColorPicker();
        this.initAvatarPreview();
        this.resetAvatarPreview();
    }

    this.assignOldImageValue = function () {
        var img = document.getElementById('user_avatar');
        var btn = document.getElementById('reset_preview_btn');

        btn.dataset.sets_image = img.src;
    }

    this.initAvatarPreview = function () {
        document.getElementById('avatar_preview_btn').addEventListener('click', function () {
            var apiUrl = this.dataset.url;
            var name = document.getElementById('name');
            var surname = document.getElementById('surname');
            var color = document.getElementById('avatar_color').value;
            var length_input = (document.getElementById('letter_length'));
            var length = (length_input.value == '') ? 2 : length_input.value;


            var queryParams = new URLSearchParams({
                name: name.value + ' ' + surname.value,
                background: color.replace('#', ''),
                bold: document.getElementById('avatar_font_bold').checked,
                length: length,
            });

            var avatarImgPreview = apiUrl + '?' + queryParams.toString();
            new Avatar().insertPreviewAvatarImage(avatarImgPreview);
        })
    }

    this.insertPreviewAvatarImage = function (avatarImgPreview) {
        var avatar_input = document.getElementsByName('avatar_preview')[0];
        var img = document.getElementById('user_avatar');
        img.src = avatarImgPreview;
        avatar_input.value = avatarImgPreview;
    }

    this.resetAvatarPreview = function () {
        document.getElementById('reset_preview_btn').addEventListener('click', function () {
            var avatar_input = document.getElementsByName('avatar_preview')[0];
            var img = document.getElementById('user_avatar');

            avatar_input.removeAttribute('value');
            img.src = this.dataset.sets_image;
        });
    }

    this.initColorPicker = function () {
        $('.avatar_color').colorpicker({
            format: 'hex'
        });


        $('.avatar_color').on('change', function () {
            var styles = {
                background: this.value,
                width: '15px',
                height: '15px'
            };

            $(this).parent('.input-group').find('.input-group-text span')
                .css(styles);
        })
    }
}
