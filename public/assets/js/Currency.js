var Currency = {
    initSearch: function () {
        var searchInput = $('#search_input');
        var searchCkxes = $('.currency_ckx');

        searchInput.on('input', function () {
            var phrase = $(this).val().toLowerCase();

            if (searchInput != '') {
                searchCkxes.hide();
                searchCkxes.filter(function () {
                    return $(this).children('label').text().trim().toLowerCase().indexOf(phrase) > -1;
                }).show();
            } else {
                searchCkxes.show();
            }
        })
    },
}
