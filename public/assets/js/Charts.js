let Charts = {
    init: function () {
        var chartData = JSON.parse(document.getElementById('data_chart').dataset.data);
        var chartDrawData = this.getDrawChartData(chartData.data);
        var ctx = document.getElementById('chart').getContext('2d');
        var config = {
            type: 'line',
            data: {
                labels: chartDrawData.dates,
                datasets: [{
                    label: chartData.dataset_title, // data
                    backgroundColor: '#ffffff',
                    borderColor: '#0034FC',
                    data: chartDrawData.prices,
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: chartData.chart_title // chart title
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: chartData.x_axe_title
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: chartData.y_axe_title
                        }
                    }]
                }
            }
        };

        new Chart(ctx, config);
    },

    getDrawChartData: function (apiData) {
        var arrPrice = [];
        var arrDates = [];

        apiData.forEach(function (value) {
            arrPrice.push(value.cena)
            arrDates.push(value.data)
        })

        return {
            prices: arrPrice,
            dates: arrDates
        }
    },

}
