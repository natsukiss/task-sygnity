let CheckOdd = {
    init: function () {
        this.initSelectPicker();
        this.initDatePicker();
        this.initForm();
    },

    initSelectPicker: function () {
        $('selectpicker').selectpicker();
    },

    initDatePicker: function () {
        var picker = $('.datepicker');
        $('.datepicker').datepicker({format: 'yyyy-mm-dd', language: picker.data('lang')});
    },

    initForm: function () {
        var form = $('#checkOddForm');

        $('#check_odd').on('click', function (e) {

            if (form[0].checkValidity()) {
                e.preventDefault();
                e.stopPropagation();

                $.get('/users/checkOdd', form.serialize(), function (data) {
                    var alert = $('.alert');
                    var currencyDataDiv = $('#currency_data')

                    if(typeof data === 'string'){
                        currencyDataDiv.html('').html(data);
                    } else {
                        $(currencyDataDiv).html('').html('<b>' + data.currency + ' : </b>' + data.rates[0].mid+' PLN')
                    }

                    alert.show();

                })
            }
        })


    }
}
