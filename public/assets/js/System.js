// bootstrap validator
(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);


    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        cache: false,
        error: function (x, e)
        {
            if(x.status != 0) {
                alert('Error '+ x.status);
            }
        },
        beforeSend: function () {
            console.log('sending....');
        },
        complete: function () {
           console.log('ajax job complete');
        },
        done: function () {
            console.log('ajax job done');
        }
    });

})();
