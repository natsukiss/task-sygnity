function User() {
    this.initDeleteAccount = function () {
        var btn = document.querySelector('#delete_account');
        if (btn) {
            var txt = JSON.parse(btn.dataset.alert);
            var form = document.querySelector('#delete_user')

            btn.addEventListener('click', function (e) {
                e.preventDefault();

                Swal.fire({
                    title: txt.title,
                    text: txt.text,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: txt.yes,
                    cancelButtonText: txt.no
                }).then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    }
                })

            })
        }

    }
}

new User().initDeleteAccount();
