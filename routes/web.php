<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/{locale}/lang', function ($locale) {
    if (!in_array($locale, config('app.supported_locales'))) {
        abort(400);
    }

    session()->put('locale', $locale);
    return redirect()->back();
})->name('locale');

Auth::routes(['verify' => true]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('users/change_password', [\App\Http\Controllers\Panel\UsersController::class, 'changePassword'])->name('users.change_password');
Route::post('users/update_password', [\App\Http\Controllers\Panel\UsersController::class, 'updatePassword'])->name('users.update_password');

Route::get('users/setup_curriences', [\App\Http\Controllers\Panel\UsersController::class, 'setupCurrencies']);
Route::post('users/setup_curriences', [\App\Http\Controllers\Panel\UsersController::class, 'saveUserCurrencies'])->name('users.setup_curriences');

Route::get('users/checkOdd', [\App\Http\Controllers\Panel\UsersController::class, 'checkOdd']);

Route::resource('users', \App\Http\Controllers\Panel\UsersController::class);

