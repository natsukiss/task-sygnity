<div class="card mt-3">
    <div class="card-header">
        <h4 class="font-weight-bold">@lang('base.chart.gold_price_main')</h4>
    </div>
    <div class="card-body">
        <div id="data_chart" data-data="{{ $chartData }}">
            <canvas id="chart"></canvas>
        </div>
    </div>
</div>
