@extends('layouts.app')

@section('styles')
    <link href="{{ asset('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<form method="POST" action="{{ $formSettings['action'] }}" class="needs-validation" novalidate>
@csrf
{{ method_field($formSettings['method_field']) }}
<div class="row">
    <div class="col-md-4 border-right">
        <div class="d-flex flex-column align-items-center text-center p-3 py-5">
            <span class="font-weight-bold">@lang('base.user.edit_avatar')</span>
            <input type="hidden" value="" name="avatar_preview">
            <img id="user_avatar" class="rounded-circle mt-5" src="{{ $user->avatar }}" width="90">
            <span class="font-weight-bold mt-2">{{ $user->fullname }}</span>
            <span class="text-black-50">{{ $user->email }}</span>
        </div>
        <div id="avatar_form">
            <div class="form-group form-inline col-12">
                <input class="form-control input-sm w-100" id="letter_length" placeholder="ilość liter w avatarze" id="inputsm" type="number" min="1" max="4">
            </div>

            <div class="form-group form-inline col-12">
                <div class="input-group w-100">
                    <input class="avatar_color form-control input-sm" id="avatar_color" type="text" value="#ddd7d7">
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <span style="background: #ddd7d7; width: 15px; height: 15px;"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group form-inline col-12">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="avatar_font_bold">
                    <label class="custom-control-label" for="avatar_font_bold">@lang('base.user.font_bold')</label>
                </div>
            </div>
            <div class="form-group form-inline col-12">
                <button data-url="{{$UIAvatarApiUrl}}" type="button" class="btn btn-success" id="avatar_preview_btn">@lang('base.user.avatar_preview')</button>
            </div>

            <div class="form-group form-inline col-12">
                <button type="button" class="btn btn-danger" id="reset_preview_btn">@lang('base.user.avatar_restore')</button>
            </div>
        </div>
    </div>

    <div class="col-md-8">
        <div class="p-3 py-5">
            <div class="d-flex justify-content-between align-items-center mb-3">
                <div class="d-flex flex-row align-items-center back"><i class="fa fa-long-arrow-left mr-1 mb-1"></i>
                    <a href="{{ route('home') }}" class="btn btn-primary">@lang('base.user.back_home')</a>
                </div>
                <h6 class="text-right">@lang('base.user.edit_profile')</h6>
            </div>
            <div class="row mt-2">
                <div class="form-group col-6">
                    <label for="name">@lang('auth.register-form.name')</label>
                    <input type="text" required class="form-control @error('name') is-invalid @enderror" name="name"  id="name" value="{{ $user->name }}" placeholder="@lang('auth.register-form.name')">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group col-6">
                    <label for="surname">@lang('auth.register-form.surname')</label>
                    <input type="text" required class="form-control @error('surname') is-invalid @enderror" name="surname" id="surname" value="{{ $user->surname }}" placeholder="@lang('auth.register-form.surname')">
                    @error('surname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="row mt-3">
                <div class="form-group col-6">
                    <label for="email">@lang('auth.register-form.e-mail')</label>
                    <input type="email" required class="form-control" name="email" id="email" value="{{ $user->email }}" placeholder="@lang('auth.register-form.e-mail')" readonly>
                </div>
                <div class="form-group col-6">
                    <label for="username">@lang('auth.register-form.username')</label>
                    <input type="text" required class="form-control" name="username" id="username" value="{{ $user->username }}" placeholder="@lang('auth.register-form.username')" readonly>
                </div>
            </div>
            <div class="mt-5 text-right"><button type="submit" class="btn btn-primary profile-button" type="button">@lang('base.user.save_profile')</button></div>
        </div>
    </div>
</div>
</form>
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/Avatar.js') }}" defer></script>
@endsection

@section('scripts-call')
    new Avatar().initAvatarGenerator()
@endsection
