<div class="card mb-3">
    <div class="card-header">
        <h4 class="font-weight-bold">@lang('base.check_odd.title')</h4>
    </div>
    <div class="card-body">
        <form id="checkOddForm" class="form-inline needs-validation" novalidate>
            <div class="form-group mb-2">
                <select name="code" required class="selectpicker" data-live-search="true">
                    @foreach ($currencies as $id => $name)
                        <option value="{{$id}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group mx-sm-3 mb-2">
                <input type="text" name="date" class="form-control datepicker" data-lang="{{app()->getLocale()}}" required autocomplete="off"/>
            </div>
            <button id="check_odd" class="btn btn-primary mb-2">
                @lang('base.check_odd.check')
            </button>
        </form>
        <div class="alert alert-info" style="display: none;">
            <div id="currency_data"></div>
        </div>

    </div>
</div>
