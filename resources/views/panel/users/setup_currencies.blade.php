@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h6 class="font-weight-bold">@lang('base.user.setup_observe_currencies')</h6>
        </div>
        <div class="card-body">
            <form method="POST" action="{{route('users.setup_curriences')}}">
                @csrf
                {{ method_field('POST') }}
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="search_input">@lang('base.user.search')</label>
                            <input type="text" class="form-control" id="search_input">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        @forelse($currencies as $currency)
                            <div class="custom-control custom-checkbox currency_ckx">
                                <input type="checkbox" class="custom-control-input" name="currencies[]" value="{{$currency->id}}" id="currency{{$currency->id}}" @if(in_array($currency->id, $user_currencies)) checked @endif>
                                <label class="custom-control-label" for="currency{{$currency->id}}">{{ $currency->name }} ({{ $currency->code }})</label>
                            </div>
                        @empty
                            @lang('base.user.save_currencies')
                        @endforelse
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <button class="btn btn-primary" type="submit">Zapisz waluty</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/js/Currency.js') }}" defer></script>
@endsection

@section('scripts-call')
    Currency.initSearch();
@endsection

