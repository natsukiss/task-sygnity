<form method="POST" action="{{ route('login') }}" class="needs-validation novalidate">
    @csrf
    <div class="form-group row">
        <label for="email" class="col-md-4 col-form-label text-md-right">
            @lang('auth.login-form.login_or_email')
        </label>

        <div class="col-md-6">
            <input id="login" type="text" class="form-control{{ $errors->has('loginFailed') ? ' is-invalid' : '' }}" name="login" value="{{ old('username') ?: old('email') }}" required autofocus>

            @if ($errors->has('loginFailed'))
                <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('loginFailed') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="password" class="col-md-4 col-form-label text-md-right">@lang('auth.login-form.password')</label>

        <div class="col-md-6">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-6 offset-md-4">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label" for="remember">
                    @lang('auth.login-form.remember_me')
                </label>
            </div>
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                @lang('auth.login-form.login_btn')
            </button>

            @if (Route::has('password.request'))
                <a class="btn btn-secondary" href="{{ route('password.request') }}">
                    @lang('auth.login-form.forgot_password')
                </a>
            @endif
        </div>
    </div>
</form>
