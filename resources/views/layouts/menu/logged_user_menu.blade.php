<li class="nav-item dropdown">
    <a id="navbarDropdown" href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="avatar rounded-circle nav-link dropdown-toggle" v-pre>
        <img alt="avatar" class="user-avatar-md img-fluid rounded-circle" src="{{ Auth::user()->avatar }}">
        {{ Auth::user()->name }}
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="{{ route('users.setup_curriences') }}"> @lang('base.user.currencies_setup') </a>
        <a class="dropdown-item" href="{{ route('users.edit', auth()->id()) }}"> @lang('base.user.edit_profile') </a>
        <a class="dropdown-item" href="{{ route('users.change_password') }}"> @lang('base.user.change_password') </a>

        <a class="dropdown-item" data-alert="{{json_encode(__('base.user.alert'))}}"  id="delete_account" href="javascript:void(0)"> @lang('base.user.delete_account') </a>
        <form id="delete_user" action="{{ route('users.destroy', auth()->id()) }}" method="POST" class="d-none">
            {{method_field('DELETE')}}
            @csrf
        </form>

        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            @lang('base.logout')
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </div>
</li>
