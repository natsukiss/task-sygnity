@if(View::hasSection('scripts-call'))
    <script>
        $(function () {
           @yield('scripts-call')
        });
    </script>
@endif
