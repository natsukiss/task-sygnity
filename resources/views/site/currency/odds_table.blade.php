<div class="card">
    <div class="card-header">
        <h4 class="font-weight-bold">
        @if(isset($logged_user))
            @lang('base.odds_table.title_logged_user')
        @else
            @lang('base.odds_table.title')
        @endif
        </h4>

        @if(count($data['data']) > 0)
        <small class="d-block">@lang('base.odds_table.title_desc', [
            'date' => $data['date'],
        ])</small>
        @endif
    </div>

    <div class="card-body">
        <table class="table table-hover">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">@lang('base.odds_table.currency')</th>
                <th scope="col">@lang('base.odds_table.code')</th>
                <th scope="col">@lang('base.odds_table.price')</th>
            </tr>
            </thead>
            <tbody>
            @forelse($data['data'] as $rate)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $rate->name }}</td>
                    <td>{{ $rate->code }}</td>
                    <td>{{ $rate->mid }}</td>
                </tr>
            @empty
                <tr>
                    <th colspan="4" scope="row">
                        @lang('base.odds_table.no_data')
                    </th>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
