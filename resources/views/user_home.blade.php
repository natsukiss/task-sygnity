@extends('layouts.app')
@section('styles')
    <link href="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">

@endsection

@section('content')
    @include('panel.users.check_odd')
    @include('site.currency.odds_table', ['logged_user' => true])
    @include('panel.users.chart')
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/charts/Chart.bundle.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/Charts.js') }}" defer></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/bootstrap-select.min.js') }}" defer></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/i18n/defaults-'.app()->getLocale().'.js') }}" defer></script>
    <script src="{{ asset('assets/js/CheckOdd.js') }}" defer></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}" defer></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/bootstrap-datepicker.pl.min.js') }}" defer></script>
@endsection

@section('scripts-call')
    Charts.init()
    CheckOdd.init()
@endsection

