<?php

return [
    'failed'   => 'Błędny login lub hasło.',
    'password' => 'Podane hasło jest nieprawidłowe.',
    'throttle' => 'Za dużo nieudanych prób logowania. Proszę spróbować za :seconds sekund.',

    // register.blade.php
    'register-form' => [
        'title' => 'Rejestracja',
        'name' => 'Imię',
        'surname' => 'Nazwisko',
        'username' => 'Login',
        'e-mail' => 'Adres e-mail',
        'password' => 'Hasło',
        'confirm-password' => 'Powtórz hasło',
        'register-button' => 'Zarejestruj',
        'registration_confirm' => 'Rejestracja przebiegła pomyślnie. Aby się zalogować należy potwierdzić maila podanego przy rejestracji: :mail'
    ],

    // login.blade.php
    'login-form' => [
        'login_or_email' => 'Login lub e-mail',
        'password' => 'Hasło',
        'remember_me' => 'Zapamiętaj mnie',
        'login_btn' => 'Zaloguj',
        'forgot_password' => 'Zapomniałem hasła'
    ],

    // email.blade.php | reset.blade.php
    'reset-password-form' => [
        'login_or_email' => 'Login lub e-mail',
        'email' => 'Adres e-mail',
        'reset_password' => 'Zresetuj hasło',
        'password' => 'Hasło',
        'confirm_password' => 'Powtórz hasło',
        'reset_pwd_btn' => 'Wyślij link do resetowania hasła'
    ],

    // VerifyEmailNotification
    'email_verify' => [
        'subject' => ':system_title - potwierdź adres e-mail',
        'line_1' => 'Kliknij przycisk poniżej, aby zweryfikować swój adres e-mail.',
        'action' => 'Potwierdź adres e-mail',
        'line_2' => 'Jeśli nie utworzyłeś konta, żadne dalsze działania nie są wymagane.',
    ],

    // ResetPasswordNotification
    'email_reset' => [
        'subject' => ':system_title - resetowanie hasła',
        'line_1' => 'Otrzymujesz tę wiadomość e-mail, ponieważ otrzymaliśmy prośbę o zresetowanie hasła do Twojego konta.',
        'action' => 'Zresetuj hasło',
        'line_2' => 'Ten link do resetowania hasła wygaśnie za :count minut.',
        'line_3' => 'Jeśli nie prosiłeś o zresetowanie hasła, nie są wymagane żadne dalsze działania.',
    ]
];
