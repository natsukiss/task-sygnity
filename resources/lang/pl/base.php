<?php
return [
    'login_nav' => 'Zaloguj się',
    'register_nav' => 'Zarejestruj się',
    'logout' => 'Wyloguj',

    'check_odd' => [
        'title' => 'Sprawdź kurs waluty z dowolnego dnia',
        'choose_currency' => 'Wybierz walutę',
        'check' => 'Sprawdź',
    ],

    'odds_table' => [
        'no_data' => 'Brak danych',
        'title' => 'Tabela kursów walut',
        'title_logged_user' => 'Tabela obserwowanych kursów walut',
        'title_desc' => 'z dnia :date (wyświetlane jest najnowsze zestawienie z NBP)',
        'currency' => 'Waluta',
        'code' => 'Kod waluty',
        'price' => 'Cena (PLN)'
    ],

    'chart' => [
        'back_days_gold' => 'KURS CENY ZLOTA Z OSTATNICH :days DNI',
        'dataset_title_back_days_gold' => 'Cena złota',
        'y_axe_title' => 'Cena (PLN)',
        'x_axe_title' => 'Data',
        'gold_price_main' => 'Cena złota'
    ],

    'user' => [
        'change_password' => 'Zmień hasło',
        'change_password_confirm' => 'Potwierdź zmianę hasła',
        'current_password' => 'Obecne hasło',
        'new_password' => 'Nowe hasło',
        'new_confirm_password' => 'Potwierdź nowe hasło',
        'current_password_not_match' => 'Podano błędne obecne hasło. Spróbuj jeszcze raz.',
        'new_password_confirm_new_password' => 'Hasła z pól nowe hasło i potwierdź nowe hasło nie są takie same.',
        'password_change_correctly' => 'Hasło zostało zmienione prawidłowo',
        'error_change_password' => 'Wystąpił błąd podczas zmiany hasła',
        'delete_account' => 'Usuń konto',
        'alert' => [
            'title' => 'Czy aby na pewno usunąć konto ? ',
            'text' => 'Tej operacji nie może przywrócić.',
            'yes' => 'Tak',
            'no' => 'Nie',
        ],
        'delete_ok' => 'Uzytkownik został usunięty',
        'delete_error' => 'Wystąpił błąd poczas usuwania użytkownika',
        'edit_profile' => 'Edytuj profil',
        'back_home' => 'Powrót do strony głównej',
        'save_profile' => 'Zapisz profil',
        'edit_profiles_success' => 'Profil został zaktualizowany',
        'edit_profile_error' => 'Wystąpił błąd podczas aktualizacji profilu',
        'edit_avatar' => 'Edytuj avatar',
        'font_bold' => 'czcionka pogrubiona',
        'avatar_preview' => 'Podgląd avatara',
        'avatar_restore' => 'Przywróć avatar',
        'currencies_setup' => 'Obserwowane waluty',
        'setup_observe_currencies' => 'Wybierz waluty, które chcesz obserwować',
        'no_curriences_in_app' => 'Brak zdefiniowanych walut',
        'save_currencies' => 'Zapisz waluty',
        'search' => 'Szukaj',
        'save_currencies_succes' => 'Poprawnie zapisano waluty'
    ],

    'email' => [
        'welcome' => 'Witaj!',
        'name' => ':system_title',
        'error' => 'Błąd',
        'regards' => 'Pozdrawiamy',
        'subcopy' => 'W przypadku problemów z kliknięciem w link ":actionText", skopiuj i wklej poniższy adres URL w przeglądarce internetowej:',
        'mail_verified_ok' => 'Mail :email został poprawnie zweryfikowany. Teraz możesz się zalogować.',
        'email_was_verified' => 'Mail :email został już zweryfikowany wcześniej.',
    ],

    'login' => [
        'successfully_login' => 'Witaj :full_name. Zostałeś pomyślnie zalogowany w systemie.',
        'successfully_logged_out' => 'Zostałeś pomyślnie wylogowany.',
        'mail_not_confirmed' => 'Adres e-mail, na który założono konto nie został potwierdzony.'
    ],

];
