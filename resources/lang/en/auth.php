<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    // register.blade.php
    'register-form' => [
        'title' => 'Register',
        'name' => 'Name',
        'surname' => 'Surame',
        'username' => 'Username',
        'e-mail' => 'E-mail address',
        'password' => 'Password',
        'confirm-password' => 'Confirm Password',
        'register-button' => 'Register',
    ],

    // login.blade.php
    'login-form' => [
        'login_or_email' => 'Login or e-mail',
        'password' => 'Password',
        'remember_me' => 'Remember me',
        'login_btn' => 'Login',
        'forgot_password' => 'Forgot your password ?'
    ],

    // email.blade.php | reset.blade.php
    'reset-password-form' => [
        'login_or_email' => 'Login or e-mail',
        'email' => 'E-mail address',
        'reset_password' => 'Reset password',
        'password' => 'Hasło',
        'confirm_password' => 'Confirm password',
        'reset_pwd_btn' => 'Send link to reset password'
    ],

    // VerifyEmailNotification
    'email_verify' => [
        'subject' => ':system_title - verify e-mail address',
        'line_1' => 'Please click the button below to verify your email address.',
        'action' => 'Verify Email Address',
        'line_2' => 'If you did not create an account, no further action is required.',
    ],

    // ResetPasswordNotification
    'email_reset' => [
        'subject' => ':system_title - password reset',
        'line_1' => 'You are receiving this email because we received a password reset request for your account.',
        'action' => 'Reset password',
        'line_2' => 'This password reset link will expire in :count minutes.',
        'line_3' => 'If you did not request a password reset, no further action is required.',
    ]



];
