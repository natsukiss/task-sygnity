<?php
return [

    'login_nav' => 'Login',
    'register_nav' => 'Register',
    'logout' => 'Logout',

    'check_odd' => [
        'title' => 'Check the currency rate from any day',
        'choose_currency' => 'Choose a currency',
        'check' => 'Check',
    ],

    'odds_table' => [
        'no_data' => 'No data',
        'title' => 'Currency exchange rate table',
        'title_logged_user' => 'Table of observed exchange rates',
        'title_desc' => 'of :date (the latest statement from NBP is displayed)',
        'currency' => 'Currency',
        'code' => 'Currency code',
        'price' => 'Price (PLN)'
    ],

    'chart' => [
        'back_days_gold' => 'GOLD PRICE COURSE FROM LATEST :days DAYS',
        'dataset_title_back_days_gold' => 'The price of gold',
        'y_axe_title' => 'Price (PLN)',
        'x_axe_title' => 'Date',
        'gold_price_main' => 'The price of gold'
    ],

    'user' => [
        'change_password' => 'Change password',
        'change_password_confirm' => 'Confirm password change',
        'current_password' => 'Current password',
        'new_password' => 'New password',
        'new_confirm_password' => 'Confirm new password',
        'current_password_not_match' => 'The wrong current password was entered. Try again.',
        'new_password_confirm_new_password' => 'The passwords in the new password fields and confirm the new password do not match.',
        'password_change_correctly' => 'Password has been changed correctly',
        'error_change_password' => 'An error occurred while changing the password',
        'delete_account' => 'Delete account',
        'alert' => [
            'title' => 'Are you sure you want to delete the account?',
            'text' => 'This operation cannot be restored.',
            'yes' => 'Yes',
            'no' => 'No',
        ],
        'delete_ok' => 'The user has been deleted',
        'delete_error' => 'An error occurred while deleting the user',
        'edit_profile' => 'Edit profile',
        'back_home' => 'Back to main page',
        'save_profile' => 'Save profile',
        'edit_profiles_success' => 'The profile has been updated',
        'edit_profile_error' => 'An error occurred while updating the profile',
        'edit_avatar' => 'Edit avatar',
        'font_bold' => 'bold font',
        'avatar_preview' => 'Avatar preview',
        'avatar_restore' => 'Restore avatar',
        'currencies_setup' => 'Watch currencies',
        'setup_observe_currencies' => 'Select the currencies you want to watch',
        'no_curriences_in_app' => 'No defined currencies',
        'save_currencies' => 'Save currencies',
        'search' => 'Search',
        'save_currencies_succes' => 'The currencies have been correctly saved'
    ],

    'email' => [
        'welcome' => 'Hello!',
        'name' => ':system_title',
        'error' => 'Error',
        'regards' => 'Regards',
        'subcopy' => 'If you’re having trouble clicking the ":actionText" button, copy and paste the URL below into your web browser:',
        'mail_verified_ok' => 'Mail :email has been successfully verified. Now you can log in.',
        'email_was_verified' => 'Mail :email has already been verified before.',

    ],

    'login' => [
        'successfully_login' => 'Hello :full_name. You have been successfully logged into the system.',
        'successfully_logged_out' => 'You have been successfully logged out of the system.',
        'mail_not_confirmed' => 'The e-mail address to which the account was created has not been confirmed.'
    ],
];
