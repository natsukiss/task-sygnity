const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css')
//     .sourceMaps();

mix.combine([
    'public/assets/css/main.css',
    'public/assets/plugins/sweetalert2/sweetalert2.min.css',
    'public/assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css',
    'public/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css',
    'public/assets/plugins/bootstrap-select/bootstrap-select.min.css',
    'public/assets/plugins/charts/Chart.min.css',
    'public/assets/plugins/sweetalert2/sweetalert2.min.css',
], 'public/css/app.css');
mix.combine([
    'public/assets/js/jquery.min.js',
    'public/assets/plugins/sweetalert2/sweetalert2.all.min.js',
    'public/assets/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js',
    'public/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
    'public/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.pl.min.js',
    'public/assets/plugins/bootstrap-select/bootstrap-select.min.js',
    'public/assets/plugins/charts/Chart.bundle.min.js',
    'public/assets/js/System.js',
    'public/assets/js/app.js',
    'public/assets/js/User.js',
    'public/assets/js/Avatar.js',
    'public/assets/js/Charts.js',
    'public/assets/js/CheckOdd.js',
    'public/assets/js/Currency.js'
], 'public/js/app.js');
