<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 31.03.2021
 * Time: 19:23
 */


namespace App\Services;


use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

final class UIAvatarService
{
    const PATH_SAVE = 'storage/users/avatars/';
    const EXTENSION = '.png';

    /**
     * user name, that two first letter get
     * @var string
     */
    private string $name;

    /**
     * API URL
     * @var string
     */
    private string $apiURL;

    private array $settings = [
        // name of user avatar
        'name' => null,
        // image size in px
        'size' => 90,
        // lenght of letters in avatar
        'length' => 2,
        // is image rounded
        'rounded' => false,
        // is font bold
        'bold' => true,
        // background color in hex
        'backgroundColor' => 'f0e9e9'
    ];

    private string $backgroundColor = 'f0e9e9';

    /**
     * @var User
     */
    private User $user;

    public function __construct()
    {
        $this->setApiURL(config('api_urls.UIAvatar'));
    }

    /**
     * @param string $apiURL
     */
    public function setApiURL(string $apiURL): void
    {
        $this->apiURL = $apiURL;
    }

    /**
     * @param int $size
     */
    public function size(int $size): void
    {
        $this->settings['size'] = $size;
    }

    /**
     * @param int $length
     */
    public function length(int $length): void
    {
        $this->settings['length'] = $length;
    }

    /**
     * @param bool $rounded
     */
    public function rounded(bool $rounded): void
    {
        $this->settings['length'] = $rounded;
    }

    /**
     * @param bool $bold
     */
    public function bold(bool $bold): void
    {
        $this->settings['bold'] = $bold;
    }

    public function saveDefaultUserAvatar(User $user)
    {
        $this->setUser($user);
        $this->setName();
        $avatarUrl = $this->generateApiUrl();
        $avatarImage = $this->file_get_contents_curl($avatarUrl);
        $this->save($avatarImage);
    }

    public function savePredefinedAvatar(User $user, $avatarUrl)
    {
        $this->setUser($user);
        $avatarImage = $this->file_get_contents_curl($avatarUrl);
        $this->save($avatarImage);
    }

    public function setName(): UIAvatarService
    {
        $this->settings['name'] = $this->getUser()->fullname;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    private function generateApiUrl()
    {
        return $this->apiURL . '?' . http_build_query($this->settings);
    }

    private function file_get_contents_curl($url): string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    private function save(string $avatar_image)
    {
        $public_path = public_path(UIAvatarService::PATH_SAVE);
        $filename = Str::random(20) . UIAvatarService::EXTENSION;

        if (!File::isDirectory($public_path)) {
            $this->checkDirectoriesPresent($public_path);
        }

        File::put($public_path . '/' . $filename, $avatar_image);
        $this->saveUserAvatar($filename);
    }

    private function saveUserAvatar(string $filename)
    {
        $this->getUser()->avatar = $filename;
        $this->getUser()->save();
    }

    private function checkDirectoriesPresent($path)
    {
        $pathinfo = (pathinfo($path));

        if (!File::isDirectory($pathinfo['dirname'])) {
            File::makeDirectory($pathinfo['dirname']);
        }

        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }

    }
}
