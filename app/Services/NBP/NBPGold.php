<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 01.04.2021
 * Time: 15:38
 */

declare(strict_types=1);

namespace App\Services\NBP;

class NBPGold extends NBPBaseService
{
    public function __construct()
    {
        parent::__construct();
    }

    # http://api.nbp.pl/api/cenyzlota/{startDate}/{endDate} #
    public function getDaysBackPrices()
    {
        $url = $this->apiUrl . '/last/' . $this->daysBack . $this->dataFormat;
        $data = $this->getData($url);

        return json_encode([
            'data' => $data,
            'chart_title' => __('base.chart.back_days_gold', ['days' => $this->daysBack]),
            'dataset_title' => __('base.chart.dataset_title_back_days_gold'),
            'x_axe_title' => __('base.chart.x_axe_title'),
            'y_axe_title' => __('base.chart.y_axe_title'),
        ]);
    }

    protected function getAddressPart(): string
    {
        return 'cenyzlota';
    }
}
