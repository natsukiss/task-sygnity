<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 01.04.2021
 * Time: 15:40
 */

declare(strict_types=1);

namespace App\Services\NBP;


class NBPCurrency extends NBPBaseService
{
    private $tables = [
        'a', 'b', 'c'
    ];

    private $dataSources = [
        'rates',
        'tables'
    ];

    public function __construct()
    {
        parent::__construct();
    }

    protected function getAddressPart(): string
    {
        return 'exchangerates';
    }

    public function getTodayCurrencies(?string $dataSource, ?string $table): object
    {
        $this->checkDataSource($dataSource);
        $this->checkTableData($table);

        $url = $this->apiUrl . '/' . $dataSource . '/' . $table . $this->dataFormat;
        return $this->getData($url)[0];
    }

    # http://api.nbp.pl/api/exchangerates/rates/{table}/code}/{date}/ #
    public function getOddValueByDay(?string $dataSource, ?string $table, ?string $code, ?string $date) {

        $this->checkDataSource($dataSource);
        $this->checkTableData($table);

        $url = $this->apiUrl .'/'. $dataSource.'/'.$table.'/'.$code.'/'.$date.$this->dataFormat;

        return $this->getData($url);
    }


    private function checkDataSource(string $dataSource)
    {
        if (!in_array($dataSource, $this->dataSources)) {
            throw new \Exception('There is no such data source in the NBP API. Available ' . implode(',', $this->dataSources));
        }
    }

    private function checkTableData(string $table)
    {
        if (!in_array($table, $this->tables)) {
            throw new \Exception('There is no such data table in the NBP API. Available ' . implode(',', $this->tables));
        }
    }
}
