<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 01.04.2021
 * Time: 15:28
 */

declare(strict_types=1);

namespace App\Services\NBP;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

abstract class NBPBaseService
{
    public string $apiUrl;
    protected int $daysBack = 10;
    protected string $addressPart;
    /**
     * json = /?format=json
     * xml = null or /?format=xml
     * @var string
     */
    protected string $dataFormat = '/?format=json';
    private Client $httpClient;

    public function __construct()
    {
        $this->getAddressPart();
        $this->setApiUrl();

        $this->httpClient = new Client();
    }

    /**
     * first part after address apiUrl/address_part/
     */
    abstract protected function getAddressPart(): string;

    private function setApiUrl()
    {
        $this->apiUrl = config('api_urls.NBP') . $this->getAddressPart();
    }

    public function setDaysBack(int $daysBack): NBPGold
    {
        $this->daysBack = $daysBack;
        return $this;
    }

    protected function getDaysBackDates(): array
    {
        $dates = [];
        $today = Carbon::today();

        $dates['todayDateString'] = $today->toDateString();
        $dates['substractDaysDate'] = $today->subDays($this->daysBack)->toDateString();

        return $dates;
    }

    protected function getData($url)
    {
        try {
            $data = $this->httpClient->get($url)->getBody()->getContents();
        } catch (RequestException $e) {
            return __('base.odds_table.no_data');
        }

        if (is_string($data)) {
            return json_decode($data);
        }

        return $data;
    }
}
