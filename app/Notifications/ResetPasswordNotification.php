<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class ResetPasswordNotification extends ResetPassword
{
    protected function buildMailMessage($url)
    {
        return (new MailMessage)
            ->subject(__('auth.email_reset.subject', ['system_title' => config('app.name')]))
            ->line(__('auth.email_reset.line_1'))
            ->action(__('auth.email_reset.action'), $url)
            ->line(__('auth.email_reset.line_2', ['count' => config('auth.passwords.' . config('auth.defaults.passwords') . '.expire')]))
            ->line(__('auth.email_reset.line_3'));
    }
}
