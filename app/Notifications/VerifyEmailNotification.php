<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class VerifyEmailNotification extends VerifyEmail
{
    protected function buildMailMessage($url)
    {
        return (new MailMessage)
            ->subject(__('auth.email_verify.subject', ['system_title' => config('app.name')]))
            ->line(__('auth.email_verify.line_1'))
            ->action(__('auth.email_verify.action'), $url)
            ->line(__('auth.email_verify.line_2'));
    }

}
