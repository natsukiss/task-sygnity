<?php

namespace App\Models;

use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyEmailNotification;
use App\Services\UIAvatarService;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmailContract
{
    use HasFactory,
        Notifiable,
        MustVerifyEmail,
        CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'root',
        'name',
        'surname',
        'email',
        'username',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function currencies()
    {
        return $this->belongsToMany(Currency::class, 'currency_user', 'user_id', 'currency_id');
    }

    public function getFullNameAttribute(): string
    {
        return $this->attributes['name'] . ' ' . $this->attributes['surname'];
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification());
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function getAvatarAttribute(): ?string
    {
        if ($this->attributes['avatar']) {
            return asset(UIAvatarService::PATH_SAVE . $this->attributes['avatar']);
        }

        return null;
    }

    public function setAvatarAttribute($value)
    {
        if (isset($this->attributes['avatar']) && $this->attributes['avatar'] !== $value) {
            @unlink(public_path(UIAvatarService::PATH_SAVE . $this->attributes['avatar']));
        }

        $this->attributes['avatar'] = $value;
    }


}
