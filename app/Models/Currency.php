<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'code', 'mid'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'currency_user', 'currency_id', 'user_id');
    }

    public function getFullCurrencyAttribute()
    {
        return $this->attributes['name'] . ' (' . $this->attributes['code'] . ')';
    }
}
