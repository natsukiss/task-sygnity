<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 30.03.2021
 * Time: 15:31
 */

declare(strict_types=1);

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface RepositoryInterface
{
    public function store(Request $request): Model;

    public function update(Request $request, int $id): Model;

    public function destroy(int $id): bool;

    public function get(int $id): Model;

}
