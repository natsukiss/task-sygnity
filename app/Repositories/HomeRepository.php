<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 02.04.2021
 * Time: 11:21
 */

declare(strict_types=1);

namespace App\Repositories;

use App\Models\Currency;
use App\Services\NBP\NBPCurrency;
use App\Services\NBP\NBPGold;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Facades\DB;

class HomeRepository
{
    private NBPCurrency $NBPCurrency;
    private NBPGold $NBPGold;

    public function __construct(NBPCurrency $NBPCurrency, NBPGold $NBPGold)
    {
        $this->NBPCurrency = $NBPCurrency;
        $this->NBPGold = $NBPGold;
    }

    public function getGoldDaysBackPrices(int $days)
    {
        return $this->NBPGold->setDaysBack($days)->getDaysBackPrices();
    }

    public function checkCurrencies(Collection $currencyQuery): array
    {
        if (!$this->checkCurrenciesAreFromToday($currencyQuery)) {
            $data = $this->NBPCurrency->getTodayCurrencies('tables', 'a');

            DB::beginTransaction();
            if ($currencyQuery->count() === 0) {
                $currencyQuery = $this->saveCurrencies($data->rates);
            } else {
                $this->updateCurrencies($data->rates);
            }
            DB::commit();
        }

        $date = $this->getOddsDate($currencyQuery);

        return [
            'data' => $currencyQuery,
            'date' => $date
        ];
    }

    private function checkCurrenciesAreFromToday(Collection $currencyQuery): bool
    {
        $obj = $currencyQuery->first();

        if (!$obj) {
            return false;
        }

        $today = Carbon::today()->toDateString();
        return ($today === $obj->updated_at->toDateString());
    }

    private function saveCurrencies(array $rates): SupportCollection
    {
        $data = [];
        foreach ($rates as $rate) {
            $data[] = Currency::create([
                'name' => $rate->currency,
                'code' => $rate->code,
                'mid' => $rate->mid,
            ]);
        }

        return collect($data);
    }

    private function updateCurrencies(array $rates)
    {
        foreach ($rates as $rate) {
            Currency::where('code', $rate->code)->first()->update([
                'mid' => $rate->mid
            ]);
        }
    }

    private function getOddsDate($currencyQuery)
    {
        return ($currencyQuery->first()) ? $currencyQuery->first()->updated_at->toDateString() : __('base.odds_table.no_data');
    }

    public function getLoggedUserOdds(): array
    {
        return [
            'data' => auth()->user()->currencies,
            'date' => $this->getOddsDate(auth()->user()->currencies)
        ];
    }

    public function getCurrenciesArray()
    {
        $arr[''] = __('base.check_odd.choose_currency');
        $currencies = Currency::get()->pluck('full_currency', 'code')->toArray();

        return $arr + $currencies;
    }
}
