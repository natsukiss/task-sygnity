<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 30.03.2021
 * Time: 15:40
 */

declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

abstract class CrudRepository implements \App\Interfaces\RepositoryInterface
{
    protected ?string $model;

    public function __construct()
    {
        $this->model = $this->setModel();
    }

    public abstract function setModel(): ?string;

    public function store(Request $request): Model
    {
        return $this->model::create($request->input());
    }

    public function update(Request $request, int $id): Model
    {
        $item = $this->model::findOrFail($id);
        $item->update($request->input());
        return $item;
    }

    public function destroy(int $id): bool
    {
        $item = $this->model::findOrFail($id);
        return $item->delete();
    }

    public function get(int $id): Model
    {
        return $this->model::findOrFail($id);
    }
}
