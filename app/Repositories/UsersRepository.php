<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 30.03.2021
 * Time: 15:46
 */

declare(strict_types=1);

namespace App\Repositories;

use App\Http\Requests\ChangePassword;
use App\Models\User;
use App\Services\NBP\NBPCurrency;
use App\Services\UIAvatarService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersRepository extends CrudRepository
{
    private UIAvatarService $avatarService;
    private NBPCurrency $NBPCurrency;

    public function __construct()
    {
        parent::__construct();
        $this->avatarService = new UIAvatarService();
        $this->NBPCurrency = new NBPCurrency();
    }

    public function setModel(): ?string
    {
        return User::class;
    }

    public function updatePassword(ChangePassword $request): bool
    {
        return $this->get(auth()->user()->id)->update([
            'password' => Hash::make($request->new_password)
        ]);
    }

    public function update(Request $request, int $id): Model
    {
        $user = $this->get($id);
        $data = $request->all();
        $avatar_preview = isset($data['avatar_preview']) ? $data['avatar_preview'] : null;

        unset($data['avatar_preview']);
        $user->update($data);

        if ($avatar_preview) {
            $this->avatarService->savePredefinedAvatar($user, $avatar_preview);
        } else {
            $this->avatarService->saveDefaultUserAvatar($user);
        }

        return $user;
    }

    public function checkOdd(NBPCurrency $NBPCurrency, array $data)
    {
        return $NBPCurrency->getOddValueByDay('rates', 'a', $data['code'], $data['date']);
    }
}
