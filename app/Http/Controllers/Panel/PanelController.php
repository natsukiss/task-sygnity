<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 30.03.2021
 * Time: 07:21
 */

declare(strict_types=1);

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;

class PanelController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }
}
