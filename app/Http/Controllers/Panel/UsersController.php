<?php

namespace App\Http\Controllers\Panel;

use App\Http\Requests\ChangePassword;
use App\Models\Currency;
use App\Models\User;
use App\Repositories\UsersRepository;
use App\Services\NBP\NBPCurrency;
use App\Services\UIAvatarService;
use Illuminate\Http\Request;

class UsersController extends PanelController
{
    private UsersRepository $repository;

    public function __construct(UsersRepository $usersRepository)
    {
        parent::__construct();
        $this->repository = $usersRepository;
    }

    public function changePassword()
    {
        $action = action([UsersController::class, 'updatePassword']);
        return view('panel.users.change_password', compact('action'));
    }

    public function updatePassword(ChangePassword $request)
    {
        if ($this->repository->updatePassword($request)) {
            return redirect(route('home'))->with('message', __('base.user.password_change_correctly'));
        }

        return redirect()->back()->with('error', __('base.user.error_change_password'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $formSettings = $this->getFormSettings($id);
        $UIAvatarApiUrl = config('api_urls.UIAvatar');
        return view('panel.users.edit', compact('user', 'formSettings', 'UIAvatarApiUrl'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateUpdate($request);

        if ($this->repository->update($request, $id)) {
            return redirect()->back()->with('message', __('base.user.edit_profiles_success'));
        }

        return redirect()->back->with('error', __('base.user.edit_profile_error'));
    }

    private function validateUpdate(Request $request)
    {
        return $this->validate($request, [
            'name' => 'required',
            'surname' => 'required'
        ], [], [
            'name' => __('auth.register-form.name'),
            'surname' => __('auth.register-form.surname'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->repository->destroy($id);
        if ($user) {
            return redirect(route('home'))->with('message', __('base.user.delete_ok'));
        }

        return redirect()->back()->with('error', __('base.user.delete_error'));
    }

    private function getFormSettings(?int $id = null): array
    {
        $settings = [];

        $method = ($id) ? 'update' : 'store';
        $settings['action'] = action([UsersController::class, $method], $id);
        $settings['method_field'] = ($id) ? 'PATCH' : 'POST';
        return $settings;
    }

    public function setupCurrencies()
    {
        $currencies = Currency::get();
        $user_currencies = auth()->user()->currencies()->pluck('currency_id')->toArray();
        return view('panel.users.setup_currencies', compact('currencies', 'user_currencies'));
    }

    public function saveUserCurrencies(Request $request)
    {
        $arr_currencies = $request->input('currencies', []);
        auth()->user()->currencies()->sync($arr_currencies);
        return redirect()->back()->with('message', __('base.user.save_currencies_succes'));
    }

    public function checkOdd(Request $request, NBPCurrency $NBPCurrency) {
        $data = $request->query();
        return response()->json($this->repository->checkOdd($NBPCurrency, $data));
    }
}
