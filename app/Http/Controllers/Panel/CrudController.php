<?php
/**
 * Created by PhpStorm
 * User: natsukiss
 * Date: 30.03.2021
 * Time: 16:50
 */

declare(strict_types=1);

namespace App\Http\Controllers\Panel;


use App\Interfaces\RepositoryInterface;

abstract class CrudController extends PanelController
{
    protected RepositoryInterface $repository;
    protected string $controller;

    public function __construct(RepositoryInterface $repository)
    {
        parent::__construct();
        $this->repository = $repository;
        $this->controller = $this->getController();
    }

    protected function formData(?int $id = null): array
    {
        $data = [];

        if ($id) {
            $data['item'] = $this->repository->get($id);
            $data['form_action'] = $this->controller . ' @update';
        } else {
            $data['form_action'] = $this->controller . ' @store';
        }

        return $data;
    }

    public function create()
    {
        return view('', $this->formData());
    }

    public function edit(int $id)
    {
        return view('', $this->formData($id));
    }

    abstract protected function getController(): string;
}
