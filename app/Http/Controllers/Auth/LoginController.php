<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    private $redirectToLogin = RouteServiceProvider::LOGIN;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest', 'language'])->except('logout');
        $this->username = $this->getAuthenticationFieldType();
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $loginType = $this->getLoginType($request);

        $request->merge([
            $loginType => $request->input('login')
        ]);

        if ($this->guard()->attempt($request->only($loginType, 'password'))) {
            return $this->sendLoginResponse($request);
        }

        $this->incrementLoginAttempts($request);
        return $this->sendFailedLoginResponse($request);

    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput()
            ->withErrors([
                'loginFailed' => __('auth.failed'),
            ]);
    }

    protected function authenticated(Request $request, $user)
    {
//        dd(__('base.login.successfully_login', ['full_name' => $user->fullname]));
        if (!$request->user()->hasVerifiedEmail()) {
            $this->guard()->logout();
            return $this->userEmailNotConfirmed();
        }

        return redirect($this->redirectTo)->with('message', __('base.login.successfully_login', ['full_name' => $user->fullname]));
    }

    protected function loggedOut(Request $request)
    {
        return redirect($this->redirectTo)->with('message', __('base.login.successfully_logged_out'));
    }

    private function userEmailNotConfirmed()
    {
        return redirect($this->redirectToLogin)->with('error', __('base.login.mail_not_confirmed'));
    }

    private function getLoginType(Request $request): string
    {
        return filter_var($request->input('login'), FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'username';
    }

    private function getAuthenticationFieldType(): string
    {
        $fieldType = $this->getLoginType(request());
        request()->merge([$fieldType => request()->input('login')]);
        return $fieldType;
    }

    public function username(): string
    {
        return $this->username;
    }

}
