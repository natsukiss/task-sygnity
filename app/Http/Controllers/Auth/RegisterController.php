<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Services\UIAvatarService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest', 'language']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param Request $request
     * @param UIAvatarService $avatarService
     * @return \Illuminate\Contracts\Validation\Validator
     * @throws ValidationException
     */

    public function register(Request $request, UIAvatarService $avatarService)
    {
        DB::beginTransaction();

        try {
            $this->validator($request->input())->validate();
            event(new Registered($user = $this->create($request->input())));
            $avatarService->saveDefaultUserAvatar($user);

            DB::commit();

            if ($response = $this->registered($request, $user)) {
                return $response;
            }

            return $request->wantsJson()
                ? new JsonResponse([], 201)
                : redirect($this->redirectPath());

        } catch (ValidationException $ve) {
            DB::rollBack();
            return redirect()->route('register')
                ->withErrors($ve->errors())
                ->withInput($request->input());
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }

        DB::commit();
    }

    public function registered(Request $request, $user)
    {
        return redirect(route('home'))->with('message', __('auth.register-form.registration_confirm', ['mail' => $user->email]));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], [], [
            'name' => __('auth.register-form.name'),
            'surname' => __('auth.register-form.surname'),
            'email' => __('auth.register-form.email'),
            'username' => __('auth.register-form.username'),
            'password' => __('auth.register-form.password'),
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'email' => $data['email'],
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
