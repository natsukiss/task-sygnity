<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use App\Repositories\HomeRepository;
use App\Services\NBP\NBPFactoryService;

class HomeController extends Controller
{
    private $homeRepository;

    public function __construct(
        HomeRepository $homeRepository
    )
    {
        parent::__construct();
        $this->homeRepository = $homeRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return $this->getStartedView();
    }

    private function getStartedView()
    {
        if (auth()->check()) {
            return $this->getLoggedUserView();
        }

        return $this->getGuestView();
    }

    private function getLoggedUserView()
    {
        $currencies = $this->homeRepository->getCurrenciesArray();
        $data = $this->homeRepository->getLoggedUserOdds();
        $chartData = $this->homeRepository->getGoldDaysBackPrices(10);
        return view('user_home', compact('chartData', 'data', 'currencies'));
    }

    private function getGuestView()
    {
        $currencyQuery = Currency::get();
        $data = $this->homeRepository->checkCurrencies($currencyQuery);

        return view('home', compact('data'));
    }
}
