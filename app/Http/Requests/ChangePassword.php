<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\MatchOldPassword;

class ChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ];
    }

    public function messages()
    {
        return [
            'new_confirm_password.same' => __('base.user.new_password_confirm_new_password')
        ];
    }

    public function attributes()
    {
        return [
            'current_password' => __('base.user.current_password'),
            'new_password' => __('base.user.new_password')
        ];
    }

}
